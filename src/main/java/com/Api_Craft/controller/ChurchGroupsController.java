/*
 * Copyright (c) 2015. The content in this file is Protected by the copyright laws of kenya and owned by Api Craft Technology.
 * Reproducing it in any way or using it without permission from Api Craft Technology will be a violation of kenyan copyrights law.
 * This may be subject to prosecution according to the kenyan law.
 */

package com.Api_Craft.controller;

import com.Api_Craft.Generators.UuidGen;
import com.Api_Craft.models.ChurchGroups;
import com.Api_Craft.service.ChurchGroupsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 * Created by Benjamin on 2/7/2015.
 */
@Controller public class ChurchGroupsController {
	private ChurchGroupsService churchGroupsService;
	
	@Autowired(required = true) @Qualifier(value = "churchGroupsService")
	public void setPersonService(ChurchGroupsService churchGroupsService1) {
		this.churchGroupsService = churchGroupsService1;
	}
	
	@RequestMapping(value = "/churchAdmin/addchurchgroups/list", method = RequestMethod.GET)
	public String listChurchGroups(Model model) {
		model.addAttribute("churchGroup", new ChurchGroups());
		model.addAttribute("listChurchGroups", this.churchGroupsService.listChurchGroups());
		return "churchGroup";
	}

	@RequestMapping(value = "/churchAdmin/addchurchgroups/save", method = RequestMethod.POST)
	public String addChurchGroup(@ModelAttribute("churchGroup") ChurchGroups churchGroups, RedirectAttributes redirectAttributes)
			throws RuntimeException {
		try {
			if (churchGroups.getChurchGroupId() == 0) {
				this.churchGroupsService.addChurchGroups(churchGroups);
				redirectAttributes.addFlashAttribute("message", "Churchgroup added successfully");
			} else {
				this.churchGroupsService.updateChurchGroups(churchGroups);
				redirectAttributes.addFlashAttribute("message", "Churchgroup updated successfully");
			}
			
		} catch (RuntimeException ex) {
			redirectAttributes.addFlashAttribute("error", "Churchgroup not saved");
		}

		return "redirect:/churchAdmin/addchurchgroups/";
	}

	@RequestMapping("/churchAdmin/addchurchgroups/list/remove/{churchGroupUuid}")
	public String removeChurchGroup(@PathVariable("churchGroupUuid") String churchGroupUuid) {

		this.churchGroupsService.removeChurchGroup(churchGroupUuid);
		return "redirect:/churchAdmin/addchurchgroups/list";
	}

	@RequestMapping("/churchAdmin/addchurchgroups/list/edit/{churchGroupUuid}")
	public String editChurchGroup(@PathVariable("churchGroupUuid") String churchGroupUuid, Model model) {
		model.addAttribute("churchGroups", this.churchGroupsService.getChurchGroupByUuid(churchGroupUuid));
		model.addAttribute("listChurchGroups", this.churchGroupsService.listChurchGroups());
		return "churchGroups";
	}
	
}
