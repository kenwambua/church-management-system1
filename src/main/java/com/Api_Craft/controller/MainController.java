/*
 * Copyright (c) 2015. The content in this file is Protected by the copyright laws of kenya and owned by Api Craft Technology.
 * Reproducing it in any way or using it without permission from Api Craft Technology will be a violation of kenyan copyrights law.
 * This may be subject to prosecution according to the kenyan law.
 */

package com.Api_Craft.controller;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.security.Principal;

@Controller public class MainController {
	private static final Logger logs = Logger.getLogger(MainController.class);

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public ModelAndView getLoginForm(@RequestParam(required = false) String authfailed, String logout, String denied,
			String sessionExpired) {

		ModelAndView model = new ModelAndView();
		if (authfailed != null) {
			model.addObject("error", "Invalid login credentials");
			logs.info("Invalid username or password, try again !");
		} else if (logout != null) {
			model.addObject("message", "You've been logged out successfully.");
			logs.info("Logged Out successfully");
		} else if (sessionExpired != null) {
			model.addObject("error", "User Session Expired Please Login to Continue");
			logs.info("User Session Expired");
		}
		if (denied != null) {
			model.addObject("error", "Access Denied! Contact System Admin");
			logs.info("User access denied");
		}

		model.setViewName("login");
		return model;
	}

	@RequestMapping(value = "/churchAdmin", method = RequestMethod.GET)
	public ModelAndView getChurchAdminPage(ModelMap modelMap, Principal principal) {
		String churchAdminName = principal.getName();
		modelMap.addAttribute("churchAdminName", churchAdminName);
		ModelAndView churchAdminModel = new ModelAndView();
		churchAdminModel.addObject(modelMap);
		churchAdminModel.setViewName("churchAdmin");
		return churchAdminModel;
	}

	@RequestMapping(value = "/schoolAdmin", method = RequestMethod.GET)
	public ModelAndView getSchoolAdmin(ModelMap modelMap, Principal principal) {
		String schoolAdminName = principal.getName();
		modelMap.addAttribute("schoolAdminName", schoolAdminName);
		ModelAndView schoolAdminModel = new ModelAndView();
		schoolAdminModel.addObject(modelMap);
		schoolAdminModel.setViewName("schoolAdmin");
		return schoolAdminModel;
	}

	@RequestMapping(value = "/error403", method = RequestMethod.GET) public String getErrorPage() {
		return "errors/error403";
	}

	@RequestMapping(value = "/churchAdmin/addchurchgroups", method = RequestMethod.GET)
	public ModelAndView addChurchGroups(ModelMap modelMap, Principal principal) {
		String churchAdminName = principal.getName();
		modelMap.addAttribute("churchAdminName", churchAdminName);
		ModelAndView churchGroupsModel = new ModelAndView();
		churchGroupsModel.addObject(modelMap);
		churchGroupsModel.setViewName("ChurchAdmin/ChurchGroups/addChurchGroups");
		return churchGroupsModel;
	}
	
	@RequestMapping(value = "/churchAdmin/viewchurchgroups", method = RequestMethod.GET)
	public ModelAndView viewChurchGroups(ModelMap modelMap, Principal principal) {
		String churchAdminName = principal.getName();
		modelMap.addAttribute("churchAdminName", churchAdminName);
		ModelAndView churchGroupsModel = new ModelAndView();
		churchGroupsModel.addObject(modelMap);
		churchGroupsModel.setViewName("ChurchAdmin/ChurchGroups/viewChurchGroups");
		return churchGroupsModel;
	}

	@RequestMapping(value = "/churchAdmin/addUsers", method = RequestMethod.GET)
	public ModelAndView addUsers(ModelMap modelMap, Principal principal) {
		String churchAdminName = principal.getName();
		modelMap.addAttribute("churchAdminName", churchAdminName);
		ModelAndView churchGroupsModel = new ModelAndView();
		churchGroupsModel.addObject(modelMap);
		churchGroupsModel.setViewName("ChurchAdmin/ChurchSysUsers/addSysUsers");
		return churchGroupsModel;
	}

	@RequestMapping(value = "/churchAdmin/viewUsers", method = RequestMethod.GET)
	public ModelAndView viewUsers(ModelMap modelMap, Principal principal) {
		String churchAdminName = principal.getName();
		modelMap.addAttribute("churchAdminName", churchAdminName);
		ModelAndView churchGroupsModel = new ModelAndView();
		churchGroupsModel.addObject(modelMap);
		churchGroupsModel.setViewName("ChurchAdmin/ChurchSysUsers/viewSysUsers");
		return churchGroupsModel;
	}

}
