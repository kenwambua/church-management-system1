/*
 * Copyright (c) 2015. The content in this file is Protected by the copyright laws of kenya and owned by Api Craft Technology.
 * Reproducing it in any way or using it without permission from Api Craft Technology will be a violation of kenyan copyrights law.
 * This may be subject to prosecution according to the kenyan law.
 */

package com.Api_Craft.controller;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by benjamin on 12/12/14.
 */
@Controller public class RedirectController {
	public static final Logger logs = Logger.getLogger(RedirectController.class);

	@RequestMapping("/default") public String loginRedirect(HttpServletRequest request) {
		if (request.isUserInRole("ROLE_CHURCHADMIN")) {
			logs.info("Admin successfully logged in");
			return "redirect:churchAdmin";
		} else if (request.isUserInRole("ROLE_SCHOOLADMIN")) {
			logs.info("User succesfully logged in");
			return "redirect:schoolAdmin";
		}
		logs.info("unknown user log in");
		return "redirect:/error403";
	}
}
