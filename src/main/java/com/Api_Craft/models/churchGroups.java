/*
 * Copyright (c) 2015. The content in this file is Protected by the copyright laws of kenya and owned by Api Craft Technology.
 * Reproducing it in any way or using it without permission from Api Craft Technology will be a violation of kenyan copyrights law.
 * This may be subject to prosecution according to the kenyan law.
 */

package com.Api_Craft.models;

import javax.persistence.*;

/**
 * Created by Benjamin on 2/7/2015.
 */
@Entity @Table(name = "churchgroups") public class ChurchGroups {
	@Id @Column(name = "churchGroupId") @GeneratedValue(strategy = GenerationType.IDENTITY) private int churchGroupId;
	private String churchGroupUuid;
	private String churchGroupName;
	private String churchGroupDesc;

	public String getChurchGroupDesc() {
		return churchGroupDesc;
	}

	public void setChurchGroupDesc(String churchGroupDesc) {
		this.churchGroupDesc = churchGroupDesc;
	}

	public String getChurchGroupUuid() {
		return churchGroupUuid;
	}

	public void setChurchGroupUuid(String churchGroupUuid) {
		this.churchGroupUuid = churchGroupUuid;
	}

	public String getChurchGroupName() {
		return churchGroupName;
	}

	public void setChurchGroupName(String churchGroupName) {
		this.churchGroupName = churchGroupName;
	}

	public int getChurchGroupId() {
		return churchGroupId;
	}

	public void setChurchGroupId(int churchGroupId) {
		this.churchGroupId = churchGroupId;
	}
	
	@Override public String toString() {
		return "churchGroupId=" + churchGroupId + ",churchGroupUuid" + churchGroupUuid + ", churchGroupName="
				+ churchGroupName + ", churchGroupDesc=" + churchGroupDesc;
	}
}
