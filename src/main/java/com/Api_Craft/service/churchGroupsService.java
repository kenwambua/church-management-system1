/*
 * Copyright (c) 2015. The content in this file is Protected by the copyright laws of kenya and owned by Api Craft Technology.
 * Reproducing it in any way or using it without permission from Api Craft Technology will be a violation of kenyan copyrights law.
 * This may be subject to prosecution according to the kenyan law.
 */

package com.Api_Craft.service;

import com.Api_Craft.models.ChurchGroups;

import java.util.List;

/**
 * Created by Benjamin on 2/7/2015.
 */
public interface ChurchGroupsService {
	public void addChurchGroups(ChurchGroups churchGroups);

	public void updateChurchGroups(ChurchGroups churchGroups);

	public List<ChurchGroups> listChurchGroups();

	public ChurchGroups getChurchGroupByUuid(String churchGroupUuid);

	public void removeChurchGroup(String churchGroupUuid);
}
