/*
 * Copyright (c) 2015. The content in this file is Protected by the copyright laws of kenya and owned by Api Craft Technology.
 * Reproducing it in any way or using it without permission from Api Craft Technology will be a violation of kenyan copyrights law.
 * This may be subject to prosecution according to the kenyan law.
 */

package com.Api_Craft.service.impl;

import com.Api_Craft.dao.ChurchGroupsDAO;
import com.Api_Craft.models.ChurchGroups;
import com.Api_Craft.service.ChurchGroupsService;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Benjamin on 2/7/2015.
 */
public class ChurchGroupsServiceImpl implements ChurchGroupsService {
	
	private ChurchGroupsDAO churchGroupsDAO;

	public void setChurchGroupsDAO(ChurchGroupsDAO churchGroupsDAO) {
		this.churchGroupsDAO = churchGroupsDAO;
	}
	
	@Override @Transactional public void addChurchGroups(ChurchGroups churchGroups) {
		this.churchGroupsDAO.addChurchGroups(churchGroups);
	}

	@Override @Transactional public void updateChurchGroups(ChurchGroups churchGroups) {
		this.churchGroupsDAO.updateChurchGroups(churchGroups);
	}

	@Override @Transactional public List<ChurchGroups> listChurchGroups() {
		return this.listChurchGroups();
	}

	@Override @Transactional public ChurchGroups getChurchGroupByUuid(String churchGroupUuid) {
		return this.getChurchGroupByUuid(churchGroupUuid);
	}

	@Override @Transactional public void removeChurchGroup(String churchGroupUuid) {
		this.churchGroupsDAO.removeChurchGroup(churchGroupUuid);
	}
}
