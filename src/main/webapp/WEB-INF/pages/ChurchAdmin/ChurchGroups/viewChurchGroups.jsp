<%--
  ~ Copyright (c) 2015. The content in this file is Protected by the copyright laws of kenya and owned by Api Craft Technology.
  ~ Reproducing it in any way or using it without permission from Api Craft Technology will be a violation of kenyan copyrights law.
  ~ This may be subject to prosecution according to the kenyan law.
  --%>

<%--
  Created by IntelliJ IDEA.
  User: Benjamin
  Date: 2/5/2015
  Time: 07:20 PM
  To change this template use File | Settings | File Templates.
--%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
	<meta charset="UTF-8">
	<title>{CHMS} | Church Groups</title>
	<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
	<%--Adding the system favicon--%>
	<link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/images/favicon.ico"
	      type="image/x-icon"/>
	<!-- bootstrap 3.0.2 -->
	<link href="${pageContext.request.contextPath}/resources/css/bootstrap.css" rel="stylesheet" type="text/css"/>
	<!-- font Awesome -->
	<link href="${pageContext.request.contextPath}/resources/css/font-awesome.min.css" rel="stylesheet"
	      type="text/css"/>
	<!-- Ionicons -->
	<link href="${pageContext.request.contextPath}/resources/css/ionicons.min.css" rel="stylesheet" type="text/css"/>
	<!-- Morris chart -->
	<link href="${pageContext.request.contextPath}/resources/css/morris/morris.css" rel="stylesheet" type="text/css"/>
	<!-- jvectormap -->
	<link href="${pageContext.request.contextPath}/resources/css/jvectormap/jquery-jvectormap-1.2.2.css"
	      rel="stylesheet"
	      type="text/css"/>
	<!-- fullCalendar -->
	<link href="${pageContext.request.contextPath}/resources/css/fullcalendar/fullcalendar.css" rel="stylesheet"
	      type="text/css"/>
	<!-- Daterange picker -->
	<link href="${pageContext.request.contextPath}/resources/css/daterangepicker/daterangepicker-bs3.css"
	      rel="stylesheet"
	      type="text/css"/>
	<!-- bootstrap wysihtml5 - text editor -->
	<link href="${pageContext.request.contextPath}/resources/css/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css"
	      rel="stylesheet" type="text/css"/>
	<!-- DATA TABLES -->
	<link href="${pageContext.request.contextPath}/resources/css/datatables/dataTables.bootstrap.css" rel="stylesheet"
	      type="text/css"/>
	<!-- Theme style -->
	<link href="${pageContext.request.contextPath}/resources/css/chms.css" rel="stylesheet" type="text/css"/>

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<script src="${pageContext.request.contextPath}/resources/js/jquery/jquery.min.js" type="text/javascript"></script>
	<script src="${pageContext.request.contextPath}/resources/js/jquery/html5shiv.js" type="text/javascript"></script>
	<script src="${pageContext.request.contextPath}/resources/js/jquery/respond.min.js" type="text/javascript"></script>
	<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
	<c:url value="/j_spring_security_logout" var="logoutUrl"/>
</head>
<body class="skin-blue">
<!-- header logo: style can be found in header.less -->
<header class="header">
	<a href='javascript:void(0);' class="logo">
		<!-- Add the class icon to your logo image or logo icon to add the margining -->
		ACK St' Martins Parish
	</a>
	<!-- Header Navbar: style can be found in header.less -->
	<nav class="navbar navbar-static-top" role="navigation">
		<!-- Sidebar toggle button-->
		<a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
			<span class="sr-only">Toggle navigation</span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		</a>

		<div class="navbar-right">
			<ul class="nav navbar-nav">
				<!-- User Account: style can be found in dropdown.less -->
				<li class="dropdown user user-menu">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						<i class="glyphicon glyphicon-user"></i>
						<span>${churchAdminName}  <i class="caret"></i></span>
					</a>
					<ul class="dropdown-menu">
						<!-- User image -->
						<li class="user-header bg-light-blue">
							<img src="${pageContext.request.contextPath}/resources/images/avatar3.png"
							     class="img-circle"
							     alt="User Image"/>

							<p>
								${churchAdminName}
						</li>
						<!-- Menu Footer-->
						<li class="user-footer">
							<div class="pull-left">
								<a href="#" class="btn btn-default btn-flat">Edit Profile</a>
							</div>
							<div class="pull-right">
								<a href="${logoutUrl}" class="btn btn-default btn-flat">Sign out</a>
							</div>
						</li>
					</ul>
				</li>
			</ul>
		</div>
	</nav>
</header>
<div class="wrapper row-offcanvas row-offcanvas-left">
	<!-- Left side column. contains the logo and sidebar -->
	<aside class="left-side sidebar-offcanvas">
		<!-- sidebar: style can be found in sidebar.less -->
		<section class="sidebar">
			<!-- Sidebar user panel -->
			<div class="user-panel">
				<div class="pull-left image">
					<img src="${pageContext.request.contextPath}/resources/images/avatar3.png" class="img-circle"
					     alt="User Image"/>
				</div>
				<div class="pull-left info">
					<p>Hello, ${churchAdminName}</p>
					<a href='javascript:void(0);'><i class="fa fa-circle text-success"></i> Logged In</a>
				</div>
			</div>
			<!-- sidebar menu: : style can be found in sidebar.less -->
			<ul class="sidebar-menu">
				<li class="active">
					<a href='javascript:void(0);'>
						<i class="fa fa-dashboard"></i> <span>Dashboard</span>
					</a>
				</li>
				<li class="treeview">
					<a href="#">
						<i class="fa fa-user"></i>
						<span>Users Management</span>
						<i class="fa fa-angle-left pull-right"></i>
					</a>
					<ul class="treeview-menu">
						<li><a href="${contextPath}/churchAdmin/addUsers"><i class="ion ion-person-add"></i> Add System users</a>
						</li>
						<li><a href="${contextPath}/churchAdmin/viewUsers"><i class="fa fa-edit"></i> View System Users</a>
						</li>
					</ul>
				</li>
				<li class="treeview">
					<a href="#">
						<i class="fa fa-users"></i>
						<span>Members Management</span>
						<i class="fa fa-angle-left pull-right"></i>
					</a>
					<ul class="treeview-menu">
						<li><a href="pages/charts/morris.html"><i class="ion ion-person-add"></i> Add Church Members</a>
						</li>
						<li><a href="pages/charts/flot.html"><i class="fa fa-edit"></i> View Church
							Members</a></li>
					</ul>
				</li>
				<li class="treeview">
					<a href="#">
						<i class="fa fa-money"></i>
						<span>Finacial Management</span>
						<i class="fa fa-angle-left pull-right"></i>
					</a>
					<ul class="treeview-menu">
						<li class="treeview">
							<a href="#">
								<i class="fa fa-dollar"></i> <span> Offertory</span>
								<i class="fa fa-angle-left pull-right"></i>
							</a>
							<ul class="treeview-menu">
								<li><a href="pages/UI/general.html"><i class="fa fa-user"></i> Add Offertory</a></li>
								<li><a href="pages/UI/icons.html"><i class="fa fa-edit"></i> View Offertory</a></li>
								<li><a href="#"><i class="fa fa-file-text"></i> Get Reports</a></li>
							</ul>
						</li>
						<li class="treeview">
							<a href="#">
								<i class="fa fa-money"></i> <span> Petty Cash</span>
								<i class="fa fa-angle-left pull-right"></i>
							</a>
							<ul class="treeview-menu">
								<li><a href="pages/UI/general.html"><i class="fa fa-plus"></i> Add Petty cash
									Records</a></li>
								<li><a href="pages/UI/icons.html"><i class="fa fa-edit"></i> View Petty Cash Record</a>
								</li>
								<li><a href="pages/UI/icons.html"><i class="fa fa-file-text"></i> Get Reports</a></li>
							</ul>
						</li>
					</ul>
				</li>
				<li class="treeview active">
					<a href="#">
						<i class="fa fa-home "></i>
						<span>Church Management</span>
						<i class="fa fa-angle-left pull-right"></i>
					</a>
					<ul class="treeview-menu">
						<li class="treeview">
							<a href="#">
								<i class="fa fa-users"></i> <span> Baptism Registration</span>
								<i class="fa fa-angle-left pull-right"></i>
							</a>
							<ul class="treeview-menu">
								<li><a href="pages/UI/general.html"><i class="fa fa-user"></i> Add person</a></li>
								<li><a href="pages/UI/icons.html"><i class="fa fa-edit"></i> View Registration</a></li>
								<li><a href="#"><i class="fa fa-money"></i> Add Registration Payment</a></li>
							</ul>
						</li>
						<li class="treeview">
							<a href="#">
								<i class="fa fa-envelope-o"></i> <span> Confirmation Registration</span>
								<i class="fa fa-angle-left pull-right"></i>
							</a>
							<ul class="treeview-menu">
								<li><a href="pages/UI/general.html"><i class="fa fa-user"></i> Add Person</a></li>
								<li><a href="pages/UI/icons.html"><i class="fa fa-file-text"></i> View Petty Cash Record</a>
								</li>
								<li><a href="pages/UI/icons.html"><i class="fa fa-money"></i> Add Confirmation
									Payment</a>
								</li>
							</ul>
						</li>
						<li class="treeview active">
							<a href="#">
								<i class="fa fa-users"></i> <span> Church Groups</span>
								<i class="fa fa-angle-left pull-right"></i>
							</a>
							<ul class="treeview-menu">
								<li><a href="${contextPath}/churchAdmin/addchurchgroups"><i
										class="fa fa-user"></i> Add Groups</a></li>
								<li class="active"><a href="${contextPath}/churchAdmin/viewchurchgroups"><i
										class="fa fa-file-text"></i> View Groups</a>
								</li>
								<li><a href="pages/UI/icons.html"><i class="fa fa-file-text-o"></i> View Group
									Members</a>
								</li>
								<li><a href="#"><i class="fa fa-briefcase"></i> Get Reports</a></li>
							</ul>
						</li>
					</ul>
				</li>
			</ul>
		</section>
		<!-- /.sidebar -->
	</aside>
	<!-- Right side column. Contains the navbar and content of the page -->
	<aside class="right-side">
		<!-- Content Header (Page header) -->
		<section class="content-header">
			<h1>
				Dashboard
				<small>Church Management</small>
			</h1>
			<ol class="breadcrumb">
				<li><a href="${contextPath}/churchAdmin/"><i class="fa fa-dashboard"></i> Home</a></li>
				<li><a href="${contextPath}/churchAdmin/"></a>Church Management</li>
				<li class="active">View Church Groups</li>
			</ol>
		</section>

		<!-- Main content -->
		<section class="content">

			<!-- Small boxes (Stat box) -->
			<div class="row">
				<div class="col-md-12">
					<div class="box box-solid">
						<div class="box-header bg-blue space">
							<h4 class="margin-left">
								<i class="fa fa-edit"></i> View Church Group</h4>
						</div>
						<div class="box-body table-responsive">
							<table id="example1" class="table table-bordered table-striped">
								<thead>
								<tr>
									<th>Rendering engine</th>
									<th>Browser</th>
									<th>Platform(s)</th>
									<th>Engine version</th>
									<th>CSS grade</th>
								</tr>
								</thead>
								<tbody>
								<tr>
									<td>Trident</td>
									<td>Internet
										Explorer 4.0
									</td>
									<td>Win 95+</td>
									<td> 4</td>
									<td>X</td>
								</tr>
								<tr>
									<td>Trident</td>
									<td>Internet
										Explorer 5.0
									</td>
									<td>Win 95+</td>
									<td>5</td>
									<td>C</td>
								</tr>
								<tr>
									<td>Trident</td>
									<td>Internet
										Explorer 5.5
									</td>
									<td>Win 95+</td>
									<td>5.5</td>
									<td>A</td>
								</tr>
								<tr>
									<td>Trident</td>
									<td>Internet
										Explorer 6
									</td>
									<td>Win 98+</td>
									<td>6</td>
									<td>A</td>
								</tr>
								<tr>
									<td>Trident</td>
									<td>Internet Explorer 7</td>
									<td>Win XP SP2+</td>
									<td>7</td>
									<td>A</td>
								</tr>
								<tr>
									<td>Trident</td>
									<td>AOL browser (AOL desktop)</td>
									<td>Win XP</td>
									<td>6</td>
									<td>A</td>
								</tr>
								<tr>
									<td>Gecko</td>
									<td>Firefox 1.0</td>
									<td>Win 98+ / OSX.2+</td>
									<td>1.7</td>
									<td>A</td>
								</tr>
								<tr>
									<td>Gecko</td>
									<td>Firefox 1.5</td>
									<td>Win 98+ / OSX.2+</td>
									<td>1.8</td>
									<td>A</td>
								</tr>
								<tr>
									<td>Gecko</td>
									<td>Firefox 2.0</td>
									<td>Win 98+ / OSX.2+</td>
									<td>1.8</td>
									<td>A</td>
								</tr>
								<tr>
									<td>Gecko</td>
									<td>Firefox 3.0</td>
									<td>Win 2k+ / OSX.3+</td>
									<td>1.9</td>
									<td>A</td>
								</tr>
								<tr>
									<td>Gecko</td>
									<td>Camino 1.0</td>
									<td>OSX.2+</td>
									<td>1.8</td>
									<td>A</td>
								</tr>
								<tr>
									<td>Gecko</td>
									<td>Camino 1.5</td>
									<td>OSX.3+</td>
									<td>1.8</td>
									<td>A</td>
								</tr>
								<tr>
									<td>Gecko</td>
									<td>Netscape 7.2</td>
									<td>Win 95+ / Mac OS 8.6-9.2</td>
									<td>1.7</td>
									<td>A</td>
								</tr>
								<tr>
									<td>Gecko</td>
									<td>Netscape Browser 8</td>
									<td>Win 98SE+</td>
									<td>1.7</td>
									<td>A</td>
								</tr>
								<tr>
									<td>Gecko</td>
									<td>Netscape Navigator 9</td>
									<td>Win 98+ / OSX.2+</td>
									<td>1.8</td>
									<td>A</td>
								</tr>
								<tr>
									<td>Gecko</td>
									<td>Mozilla 1.0</td>
									<td>Win 95+ / OSX.1+</td>
									<td>1</td>
									<td>A</td>
								</tr>
								<tr>
									<td>Gecko</td>
									<td>Mozilla 1.1</td>
									<td>Win 95+ / OSX.1+</td>
									<td>1.1</td>
									<td>A</td>
								</tr>
								<tr>
									<td>Gecko</td>
									<td>Mozilla 1.2</td>
									<td>Win 95+ / OSX.1+</td>
									<td>1.2</td>
									<td>A</td>
								</tr>
								<tr>
									<td>Gecko</td>
									<td>Mozilla 1.3</td>
									<td>Win 95+ / OSX.1+</td>
									<td>1.3</td>
									<td>A</td>
								</tr>
								<tr>
									<td>Gecko</td>
									<td>Mozilla 1.4</td>
									<td>Win 95+ / OSX.1+</td>
									<td>1.4</td>
									<td>A</td>
								</tr>
								<tr>
									<td>Gecko</td>
									<td>Mozilla 1.5</td>
									<td>Win 95+ / OSX.1+</td>
									<td>1.5</td>
									<td>A</td>
								</tr>
								<tr>
									<td>Gecko</td>
									<td>Mozilla 1.6</td>
									<td>Win 95+ / OSX.1+</td>
									<td>1.6</td>
									<td>A</td>
								</tr>
								<tr>
									<td>Gecko</td>
									<td>Mozilla 1.7</td>
									<td>Win 98+ / OSX.1+</td>
									<td>1.7</td>
									<td>A</td>
								</tr>
								<tr>
									<td>Gecko</td>
									<td>Mozilla 1.8</td>
									<td>Win 98+ / OSX.1+</td>
									<td>1.8</td>
									<td>A</td>
								</tr>
								<tr>
									<td>Gecko</td>
									<td>Seamonkey 1.1</td>
									<td>Win 98+ / OSX.2+</td>
									<td>1.8</td>
									<td>A</td>
								</tr>
								<tr>
									<td>Gecko</td>
									<td>Epiphany 2.20</td>
									<td>Gnome</td>
									<td>1.8</td>
									<td>A</td>
								</tr>
								<tr>
									<td>Webkit</td>
									<td>Safari 1.2</td>
									<td>OSX.3</td>
									<td>125.5</td>
									<td>A</td>
								</tr>
								<tr>
									<td>Webkit</td>
									<td>Safari 1.3</td>
									<td>OSX.3</td>
									<td>312.8</td>
									<td>A</td>
								</tr>
								<tr>
									<td>Webkit</td>
									<td>Safari 2.0</td>
									<td>OSX.4+</td>
									<td>419.3</td>
									<td>A</td>
								</tr>
								<tr>
									<td>Webkit</td>
									<td>Safari 3.0</td>
									<td>OSX.4+</td>
									<td>522.1</td>
									<td>A</td>
								</tr>
								<tr>
									<td>Webkit</td>
									<td>OmniWeb 5.5</td>
									<td>OSX.4+</td>
									<td>420</td>
									<td>A</td>
								</tr>
								<tr>
									<td>Webkit</td>
									<td>iPod Touch / iPhone</td>
									<td>iPod</td>
									<td>420.1</td>
									<td>A</td>
								</tr>
								<tr>
									<td>Webkit</td>
									<td>S60</td>
									<td>S60</td>
									<td>413</td>
									<td>A</td>
								</tr>
								<tr>
									<td>Presto</td>
									<td>Opera 7.0</td>
									<td>Win 95+ / OSX.1+</td>
									<td>-</td>
									<td>A</td>
								</tr>
								<tr>
									<td>Presto</td>
									<td>Opera 7.5</td>
									<td>Win 95+ / OSX.2+</td>
									<td>-</td>
									<td>A</td>
								</tr>
								<tr>
									<td>Presto</td>
									<td>Opera 8.0</td>
									<td>Win 95+ / OSX.2+</td>
									<td>-</td>
									<td>A</td>
								</tr>
								<tr>
									<td>Presto</td>
									<td>Opera 8.5</td>
									<td>Win 95+ / OSX.2+</td>
									<td>-</td>
									<td>A</td>
								</tr>
								<tr>
									<td>Presto</td>
									<td>Opera 9.0</td>
									<td>Win 95+ / OSX.3+</td>
									<td>-</td>
									<td>A</td>
								</tr>
								<tr>
									<td>Presto</td>
									<td>Opera 9.2</td>
									<td>Win 88+ / OSX.3+</td>
									<td>-</td>
									<td>A</td>
								</tr>
								<tr>
									<td>Presto</td>
									<td>Opera 9.5</td>
									<td>Win 88+ / OSX.3+</td>
									<td>-</td>
									<td>A</td>
								</tr>
								<tr>
									<td>Presto</td>
									<td>Opera for Wii</td>
									<td>Wii</td>
									<td>-</td>
									<td>A</td>
								</tr>
								<tr>
									<td>Presto</td>
									<td>Nokia N800</td>
									<td>N800</td>
									<td>-</td>
									<td>A</td>
								</tr>
								<tr>
									<td>Presto</td>
									<td>Nintendo DS browser</td>
									<td>Nintendo DS</td>
									<td>8.5</td>
									<td>C/A<sup>1</sup></td>
								</tr>
								<tr>
									<td>KHTML</td>
									<td>Konqureror 3.1</td>
									<td>KDE 3.1</td>
									<td>3.1</td>
									<td>C</td>
								</tr>
								<tr>
									<td>KHTML</td>
									<td>Konqureror 3.3</td>
									<td>KDE 3.3</td>
									<td>3.3</td>
									<td>A</td>
								</tr>
								<tr>
									<td>KHTML</td>
									<td>Konqureror 3.5</td>
									<td>KDE 3.5</td>
									<td>3.5</td>
									<td>A</td>
								</tr>
								<tr>
									<td>Tasman</td>
									<td>Internet Explorer 4.5</td>
									<td>Mac OS 8-9</td>
									<td>-</td>
									<td>X</td>
								</tr>
								<tr>
									<td>Tasman</td>
									<td>Internet Explorer 5.1</td>
									<td>Mac OS 7.6-9</td>
									<td>1</td>
									<td>C</td>
								</tr>
								<tr>
									<td>Tasman</td>
									<td>Internet Explorer 5.2</td>
									<td>Mac OS 8-X</td>
									<td>1</td>
									<td>C</td>
								</tr>
								<tr>
									<td>Misc</td>
									<td>NetFront 3.1</td>
									<td>Embedded devices</td>
									<td>-</td>
									<td>C</td>
								</tr>
								<tr>
									<td>Misc</td>
									<td>NetFront 3.4</td>
									<td>Embedded devices</td>
									<td>-</td>
									<td>A</td>
								</tr>
								<tr>
									<td>Misc</td>
									<td>Dillo 0.8</td>
									<td>Embedded devices</td>
									<td>-</td>
									<td>X</td>
								</tr>
								<tr>
									<td>Misc</td>
									<td>Links</td>
									<td>Text only</td>
									<td>-</td>
									<td>X</td>
								</tr>
								<tr>
									<td>Misc</td>
									<td>Lynx</td>
									<td>Text only</td>
									<td>-</td>
									<td>X</td>
								</tr>
								<tr>
									<td>Misc</td>
									<td>IE Mobile</td>
									<td>Windows Mobile 6</td>
									<td>-</td>
									<td>C</td>
								</tr>
								<tr>
									<td>Misc</td>
									<td>PSP browser</td>
									<td>PSP</td>
									<td>-</td>
									<td>C</td>
								</tr>
								<tr>
									<td>Other browsers</td>
									<td>All others</td>
									<td>-</td>
									<td>-</td>
									<td>U</td>
								</tr>
								</tbody>
								<tfoot>
								<tr>
									<th>Rendering engine</th>
									<th>Browser</th>
									<th>Platform(s)</th>
									<th>Engine version</th>
									<th>CSS grade</th>
								</tr>
								</tfoot>
							</table>
						</div>
						<!-- /.box-body -->
					</div>
				</div>
			</div>
		</section>
		<!-- /.content -->
	</aside>
	<!-- /.right-side -->
</div>
<!-- ./wrapper -->

<!-- add new calendar event modal -->


<!-- jQuery 2.0.2 -->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
<!-- jQuery UI 1.10.3 -->
<script src="${pageContext.request.contextPath}/resources/js/jquery-ui-1.10.3.min.js" type="text/javascript"></script>
<!-- Bootstrap -->
<script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js" type="text/javascript"></script>
<!-- Morris.js charts -->
<script src="//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/plugins/morris/morris.min.js"
        type="text/javascript"></script>
<!-- Sparkline -->
<script src="${pageContext.request.contextPath}/resources/js/plugins/sparkline/jquery.sparkline.min.js"
        type="text/javascript"></script>
<!-- jvectormap -->
<script src="${pageContext.request.contextPath}/resources/js/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"
        type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/resources/js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"
        type="text/javascript"></script>
<!-- fullCalendar -->
<script src="${pageContext.request.contextPath}/resources/js/plugins/fullcalendar/fullcalendar.min.js"
        type="text/javascript"></script>
<!-- jQuery Knob Chart -->
<script src="${pageContext.request.contextPath}/resources/js/plugins/jqueryKnob/jquery.knob.js"
        type="text/javascript"></script>
<!-- daterangepicker -->
<script src="${pageContext.request.contextPath}/resources/js/plugins/daterangepicker/daterangepicker.js"
        type="text/javascript"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="${pageContext.request.contextPath}/resources/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"
        type="text/javascript"></script>
<!-- iCheck -->
<script src="${pageContext.request.contextPath}/resources/js/plugins/iCheck/icheck.min.js"
        type="text/javascript"></script>
<!-- DATA TABES SCRIPT -->
<script src="${pageContext.request.contextPath}/resources/js/plugins/datatables/jquery.dataTables.js"
        type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/resources/js/plugins/datatables/dataTables.bootstrap.js"
        type="text/javascript"></script>
<!-- chms App -->
<script src="${pageContext.request.contextPath}/resources/js/chms/app.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/resources/js/chms/dashboard.js" type="text/javascript"></script>
<!-- page script -->
<script type="text/javascript">
	$(function () {
		$("#example1").dataTable();
		$('#example2').dataTable({
			"bPaginate": true,
			"bLengthChange": false,
			"bFilter": false,
			"bSort": true,
			"bInfo": true,
			"bAutoWidth": false
		});
	});
</script>

</body>
</html>
