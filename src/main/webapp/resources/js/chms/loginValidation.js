/*
 * Copyright (c) 2015. The content in this file is Protected by the copyright laws of kenya and owned by Api Craft Technology.
 * Reproducing it in any way or using it without permission from Api Craft Technology will be a violation of kenyan copyrights law.
 * This may be subject to prosecution according to the kenyan law.
 */

/**
 * Created by Benjamin on 1/27/2015.
 */
$(document).ready(function () {
	$("#Login").click(function () {
		var email = $("#staffUsername").val();
		var password = $("#staffPassword").val();
// Checking for blank fields.
		if (email == '' || password == '') {
			$('input[type="text"],input[type="password"]').css("border", "2px solid red");
			$('input[type="text"],input[type="password"]').css("box-shadow", "0 0 3px red");
			alert("Please fill all fields...!!!!!!");
		}
	});
});
