/*
 * Copyright (c) 2015. The content in this file is Protected by the copyright laws of kenya and owned by Api Craft Technology.
 * Reproducing it in any way or using it without permission from Api Craft Technology will be a violation of kenyan copyrights law.
 * This may be subject to prosecution according to the kenyan law.
 */

(function ($) {
	$.extend($.inputmask.defaults.aliases, {
		'phone': {
			url: "phone-codes/phone-codes.json",
			mask: function (opts) {
				opts.definitions = {
					'p': {
						validator: function () {
							return false;
						},
						cardinality: 1
					},
					'#': {
						validator: "[0-9]",
						cardinality: 1
					}
				};
				var maskList = [];
				$.ajax({
					url: opts.url,
					async: false,
					dataType: 'json',
					success: function (response) {
						maskList = response;
					}
				});

				maskList.splice(0, 0, "+p(ppp)ppp-pppp");
				return maskList;
			}
		}
	});
})(jQuery);
